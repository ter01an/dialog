<?php

namespace Teran\Dialog\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Teran\Dialog\Events\ChatMessage;
use Teran\Dialog\Events\NewMessage;
use Teran\Dialog\Events\ViewedMessage;
use Teran\Dialog\Models\DialogMark;
use Teran\Dialog\Models\DialogMessage;

use App\User;
use Teran\Dialog\Models\DialogMessageAttachment;

class TaskControllers extends Controller
{

    public function markAsViewed(Request $request, $message_id) {
        $user = $request->user();
        $message = DialogMessage::find($message_id);
        if($message->receive_id == $user->id) {
            $message->viewed_at = date('Y-m-d H:i:s');
            $message->save();

            event(new ViewedMessage($user->id));
        }
        return response()->json(['status' => 'success']);
    }

    public function markAsDelete(Request $request, $user_id, $receive_id, $group_id) {
        $mark = DialogMark::where('user_id', $user_id)->where('receive_id', $receive_id)->where('group_id', $group_id)->first();
        if($mark) {
            $mark->deleted_at = date('Y-m-d H:i:s');
            $mark->save();
        } else {
            $mark = new DialogMark();
            $mark->user_id = $user_id;
            $mark->receive_id = $receive_id;
            $mark->group_id = $group_id;
            $mark->deleted_at = date('Y-m-d H:i:s');
            $mark->save();
        }
        return $request->has('back') ? redirect()->to($request->get('back')) : redirect()->back();
    }

    public function markAsSpam(Request $request, $user_id, $receive_id, $group_id) {
        $mark = DialogMark::where('user_id', $user_id)->where('receive_id', $receive_id)->where('group_id', $group_id)->first();
        if($mark) {
            $mark->spam_at = date('Y-m-d H:i:s');
            $mark->save();
        } else {
            $mark = new DialogMark();
            $mark->user_id = $user_id;
            $mark->receive_id = $receive_id;
            $mark->group_id = $group_id;
            $mark->spam_at = date('Y-m-d H:i:s');
            $mark->save();
        }
        return $request->has('back') ? redirect()->to($request->get('back')) : redirect()->back();
    }

    public function sendMessage(Request $request) {
        $user = $request->user();
        $receive_id = (int)$request->get('receive_id', 0);
        $group_id = (int)$request->get('group_id', 0);
        $text = $request->get('text', '');

        if($receive_id && $receive_id !== $user->id && $text) {
            $message = new DialogMessage();
            $message->text          = $request->get('text', '');
            $message->user_id       = $user->id;
            $message->receive_id    = $receive_id;
            $message->group_id      = $group_id;
            $message->save();

            if($request->hasFile('attachments')) {
                foreach ($request->attachments as $attachment) {
                    $path = $attachment->store('uploads');
                    $message_attachment = new DialogMessageAttachment();
                    $message_attachment->dialog_message_id = $message->id;
                    $message_attachment->name = $attachment->getClientOriginalName();
                    $message_attachment->file = $path;
                    $message_attachment->save();
                }
            }
            DialogMark::refreshDelete($message->receive_id, $message->user_id, $message->group_id);

            event(new ChatMessage($message));
            event(new NewMessage($message->receive_id));

            return $message->messageData();
        }
    }

    public function getMessage(Request $request) {
        $user = $request->user();

        $receive_id = (int)$request->get('receive_id', 0);
        $group_id = (int)$request->get('group_id', 0);

        if($user) {
            $items =
                DialogMessage::where(function ($query) use ($user, $receive_id) {
                    $query->where('user_id', $user->id)->orWhere('user_id', $receive_id);
                })->where(function ($query) use ($user, $receive_id) {
                    $query->where('receive_id', $user->id)->orWhere('receive_id', $receive_id);
                })->where('group_id', $group_id)->orderBy('created_at', 'asc')->get();
            $result = [];
            foreach ($items as $item) {
                $result[] = $item->messageData();
            }
            return $result;
        }
        return [];
    }

    public function getAttachment(Request $request, $attachment_id) {
        $attachment = DialogMessageAttachment::find($attachment_id);
        if($attachment) {
            return Storage::download($attachment->file, $attachment->name);
        }
        abort(404);
    }

    public function updateToken(Request $request)
    {
        $token = Str::random(60);

        $request->user()->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();

        return ['token' => $token];
    }
}
