<?php

namespace Teran\Dialog\Events;

use App\User;
use Illuminate\Support\Facades\DB;
use Teran\Dialog\Models\DialogMessage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    protected $user_id;

    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('message.' . $this->user_id);
    }

    public function broadcastWith()
    {
        $counts = DB::table('dialog_messages')
                ->select([DB::raw('COUNT(id) as count'), 'group_id', 'receive_id'])
                ->groupBy('group_id')
                ->where('receive_id', $this->user_id)
                ->where('viewed_at', null)->get();
        return [
            'counts' => $counts
        ];
    }

    public function broadcastAs()
    {
        return 'dialog.new';
    }
}
