<?php

namespace Teran\Dialog\Events;

use App\User;
use Teran\Dialog\Models\DialogMessage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChatMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    protected $message;

    public function __construct(DialogMessage $message)
    {
        $this->message = $message;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('chat.' . $this->message->group_id . '.' . $this->message->user_id . '.' . $this->message->receive_id);
    }

    public function broadcastWith()
    {
        return $this->message->messageData();
    }

    public function broadcastAs()
    {
        return 'dialog.message';
    }
}
