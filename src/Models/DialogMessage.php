<?php

namespace Teran\Dialog\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DialogMessage extends Model
{
    use SoftDeletes;

    public function attachments() {
        return $this->hasMany(DialogMessageAttachment::class);
    }

    public static function countNewMessages($user_id, $group_id = false) {
        if($group_id) {
            return self::where('receive_id', $user_id)->where('group_id', $group_id)->where('viewed_at', null)->count();
        }
        return self::where('receive_id', $user_id)->where('viewed_at', null)->count();
    }

    public function messageData() {
        $user = User::find($this->user_id);
        $receive = User::find($this->receive_id);

        $attachments = [];
        foreach ($this->attachments as $attachment) {
            $info = pathinfo(storage_path($attachment->file));
            $attachments[] = [
                'name'  => $attachment->name,
                'ext'   => $info['extension'],
                'file'  => route('dialog.get.attachment', ['attachment_id' => $attachment->id])
            ];
        }
        return [
            'id'            => $this->id,
            'from'          => [
                'id'        => $this->id,
                'name'      => $user->name,
                'avatar'    => $user && method_exists($user, 'getAvatar') ? $user->getAvatar() : ""
            ],
            'to'            => [
                'id'        => $receive ? $receive->id : $this->receive_id,
                'name'      => $receive ? $receive->name : "",
                'avatar'    => $receive && method_exists($receive, 'getAvatar') ? $user->getAvatar() : ""
            ],
            'text'          => $this->text,
            'attachments'   => $attachments,
            'viewed_at'     => $this->viewed_at ? strtotime($this->viewed_at) * 1000 : false, // convert into milliseconds for js date
            'created_at'    => strtotime($this->created_at) * 1000 // convert into milliseconds for js date
        ];
    }
}
