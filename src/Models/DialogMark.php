<?php

namespace Teran\Dialog\Models;

use Illuminate\Database\Eloquent\Model;

class DialogMark extends Model
{
    public $timestamps = false;

    public static function isSpam($user_id, $receive_id, $group_id) {
        $mark = DialogMark::where('user_id', $user_id)->where('receive_id', $receive_id)->where('group_id', $group_id)->first();
        return ($mark && $mark->spam_at != null);
    }

    public static function isDelete($user_id, $receive_id, $group_id) {
        $mark = DialogMark::where('user_id', $user_id)->where('receive_id', $receive_id)->where('group_id', $group_id)->first();
        return ($mark && $mark->deleted_at != null);
    }

    public static function refreshDelete($user_id, $receive_id, $group_id) {
        $mark = DialogMark::where('user_id', $user_id)->where('receive_id', $receive_id)->where('group_id', $group_id)->first();
        if($mark) {
            $mark->deleted_at = null;
            $mark->save();
        }
    }
}
