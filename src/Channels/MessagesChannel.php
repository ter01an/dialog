<?php

namespace Teran\Dialog\Channels;

use App\User;

class MessagesChannel
{
    public function join(User $user, int $group_id, int $from_id, int $to_id) {
        return $from_id !== $to_id;
    }
}
