<?php

namespace Teran\Dialog;

use Illuminate\Support\ServiceProvider;

class DialogServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../migrations/');
        $this->loadRoutesFrom(__DIR__ . '/../routes/channels.php');
        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->mergeConfigFrom(
            __DIR__ . '/../config/dialog.php', 'dialog'
        );
        $this->publishes([
            __DIR__ . '/../public' => public_path('vendor/dialog'),
        ], 'public');
    }

    public function register()
    {

    }
}
