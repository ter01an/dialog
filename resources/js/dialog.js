import DialogSimple from "./components/DialogSimple.svelte";

/* Require */
window._        = require('lodash');
window.io       = require('socket.io-client');
window.axios    = require('axios');

/* Plugins settings */
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

let items = document.getElementsByClassName("dialog-simple");
_.each(items, element => {
    const app = new DialogSimple({
        target: element,
        props: {
            api_token: element.getAttribute('data-user-token'),
            user_id: element.getAttribute('data-user-id'),
            receive_id: element.getAttribute('data-receive-id'),
            group_id: element.getAttribute('data-group-id'),
            attachment_enable: element.getAttribute('data-attachment-enable') ? true : false
        }
    });
});

require('./visor');
