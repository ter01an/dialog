import Echo from "laravel-echo";

if('user_id' in window) {
    let echo = new Echo({
        broadcaster: 'socket.io',
        host: window.location.hostname + ':6001'
    });

    let visors = document.getElementsByClassName('dialog-visors');
    let total_visors = document.getElementsByClassName('dialog-total-visors');

    function sound() {
        let audio = new Audio();
        audio.src = '/poop.mp3';
        audio.autoplay = true;
    }

    function change(e) {
        let total = 0,
            groups = {};
        _.each(e.counts, item => {
            total += parseInt(item.count);
            groups[item.group_id] = item.count;
        });
        _.each(visors, item => {
            let group_id = item.getAttribute('data-group-id');
            if(group_id in groups) {
                item.innerText = groups[group_id];
            }
        });
        _.each(total_visors, item => {
            item.innerText = total;
        });
    }

    echo.private(`message.${window.user_id}`)
        .listen('.dialog.new', (e) => {
            change(e);
            sound();
        });

    echo.private(`viewed.${window.user_id}`)
        .listen('.dialog.viewed', (e) => {
            change(e);
        });
}
