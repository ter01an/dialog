const mix = require('laravel-mix');

require('laravel-mix-svelte');

mix.js('resources/js/dialog.js', 'public/js')
    .sass('resources/scss/dialog.scss', 'public/css')
    .svelte();
