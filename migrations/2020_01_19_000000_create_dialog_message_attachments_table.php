<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDialogMessageAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dialog_message_attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('dialog_message_id');
            $table->char('name', 255);
            $table->char('file', 255);
            $table->timestamps();
            $table->index('message_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dialog_message_attachments');
    }
}
