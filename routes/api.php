<?php

Route::middleware('auth:api')->group(function() {
    Route::get('/dialog/get/messages', 'Teran\Dialog\Http\Controllers\TaskControllers@getMessage');
    Route::post('/dialog/send/message', 'Teran\Dialog\Http\Controllers\TaskControllers@sendMessage');
    
    Route::get('/dialog/mark/viewed/{message_id}', 'Teran\Dialog\Http\Controllers\TaskControllers@markAsViewed');
});
