<?php

Route::get('/dialog/get/attachment/{attachment_id}', 'Teran\Dialog\Http\Controllers\TaskControllers@getAttachment')->name('dialog.get.attachment');
Route::get('/dialog/mark/spam/{user_id}/{receive_id}/{group_id}', 'Teran\Dialog\Http\Controllers\TaskControllers@markAsSpam')->name('dialog.mark.spam');
Route::get('/dialog/mark/delete/{user_id}/{receive_id}/{group_id}', 'Teran\Dialog\Http\Controllers\TaskControllers@markAsDelete')->name('dialog.mark.delete');
