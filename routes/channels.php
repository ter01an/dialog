<?php

Broadcast::channel('chat.{group_id}.{from_id}.{to_id}', \Teran\Dialog\Channels\MessagesChannel::class);
Broadcast::channel('message.{user_id}', \Teran\Dialog\Channels\NewMessagesChannel::class);
Broadcast::channel('viewed.{user_id}', \Teran\Dialog\Channels\ViewedMessagesChannel::class);
